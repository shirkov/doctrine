<?php
require "config/bootstrap.php";


$book = new Book('In Search of Lost Time');
$author = new Author('Marcel Proust');
$book->setAuthor($author);
$author->setBook($book);

$entityManager->persist($book);
$entityManager->persist($author);
$entityManager->flush();